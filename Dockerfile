FROM williamyeh/ansible:ubuntu16.04
RUN apt-get update && apt-get install -y curl unzip git
RUN curl -o packer.zip https://releases.hashicorp.com/packer/1.3.2/packer_1.3.2_linux_amd64.zip
RUN unzip -d /usr/local/bin packer.zip && chmod +x /usr/local/bin/packer && rm packer.zip